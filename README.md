<!--
SPDX-FileCopyrightText: 2018 yuzu Emulator Project
SPDX-License-Identifier: GPL-2.0-or-later
-->

<h1 align="center">
  <br>
  <b>yuzu</b>
  <br>
</h1>

<h4 align="center"><b>yuzu</b> is the world's most popular, open-source, Nintendo Switch emulator — started by the creators of <a href="https://citra-emu.org" target="_blank">Citra</a>.
<br>
It is written in C++ with portability in mind.
</h4>

> [!NOTE]
> This is an unofficial mirror fork of the original repository.

<p align="center">
  <a href="#development">Development</a> |
  <a href="#license">License</a>
</p>

 - Fixed controller UI being cut off at the bottom
## Development

Most of the development happens on Codeberg. It's also where [our central repository](https://codeberg.org/yuzu-emu/yuzu) is hosted.

Git history will be rewritten frequently as patches are added and removed from other forks


## License

yuzu is licensed under the GPLv3 (or any later version). Refer to the [LICENSE.txt](https://codeberg.org/yuzu-emu/yuzu/raw/branch/main/LICENSE.txt) file.
